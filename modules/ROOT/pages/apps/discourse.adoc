= Discourse
:sectnums:
:toc:
:toclevels: 3
:repo: https://gitlab.com/docker-scripts/discourse
:repo-blob: {repo}/-/blob/master
:repo-raw: {repo}/-/raw/master

== The scripts

The scripts are located at: {repo} +
We can get them with [cmd]`ds pull`:

[subs="+quotes"]
....
[prompt]*root@vps ~ #* [in]*ds pull discourse*
[prompt]*root@vps ~ #* [in]*tree /opt/docker-scripts/discourse/*
/opt/docker-scripts/discourse/
discourse/
├── cmd
│   ├── backup.sh
│   ├── build.sh
│   ├── clone.sh
│   ├── config.sh
│   ├── copy-ssl-cert.sh
│   ├── create.sh
│   ├── discourse.sh
│   ├── restore.sh
│   └── upgrade.sh
├── ds.sh
├── LICENSE
├── misc
│   └── init.sh
├── README.md
└── settings.sh
....

The official Discourse release uses its own Docker framework for
installing everything that is needed for the application. So, our
scripts are just a shallow wrapper to this framework. We just
customize a few settings to make it fit with the rest of
docker-scripts apps (for example placing it behind `revproxy` ).

=== Make

Because Discourse uses its own image and its own docker framework
(which, by the way, is not docker-compose), we don't need a
[path]`Dockerfile`. We also override the commands `build`, `create`
and `config` to make sure that they do nothing:

****
.{repo-blob}/cmd/build.sh[[path]`cmd/build.sh`^]
[%collapsible%open]
====
[source,bash]
----
cmd_build() {
    # do nothing
    :
}
----
====

.{repo-blob}/cmd/create.sh[[path]`cmd/create.sh`^]
[%collapsible%open]
====
[source,bash]
----
cmd_create() {
    # do nothing
    :
}
----
====

.{repo-blob}/cmd/config.sh[[path]`cmd/config.sh`^]
[%collapsible%open]
====
[source,bash]
----
cmd_config() {
    # do nothing
    :
}
----
====
****

When the app directory is initialized, the command [cmd]`ds init`,
among other things, calls as well the script [path]`misc/init.sh`, if
available:

.{repo-blob}/misc/init.sh[[path]`cmd/config.sh`^]
[%collapsible%open]
====
[source,bash]
----
# run custom init steps
git clone https://github.com/discourse/discourse_docker.git
----

It just clones the official discourse repo inside the directory.
====

Then, to make the application, the config file [path]`settings.sh` and
the script [path]`ds.sh` are used:

****
.{repo-blob}/settings.sh[[path]`settings.sh`^]
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/settings.sh[]
----
====

.{repo-blob}/ds.sh[[path]`ds.sh`^]
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/ds.sh[]
----
====
****

The file {repo-blob}/ds.sh[[path]`ds.sh`^] is loaded automatically by
the framework, when present, and can be used to override some
framework functions, or to define new auxiliary functions. In this
case, it is redefining `cmd_start()`, `cmd_stop()`, etc. in terms of
the command [cmd]`launcher` inside [path]`discourse_docker/`, which is
the official tool for managing the discourse containers.  Two
containers are managed: one for the app itself and one called
`mail-receiver`.

The most important function is `cmd_make()`, which does all the
building and setup:

* add a revproxy domain and get a ssl-cert
* copy the app and mail-receiver config files
* build the app and mail-receiver containers
* create cron jobs, and make other configurations

Configuration files for the app and mail-receiver containers are
copied from [path]`discourse_docker/samples/standalone.yml` and
[path]`discourse_docker/samples/mail-receiver.yml` to
[path]`discourse_docker/containers/` and then modified/customized.

NOTE: They are copied only if they don't exist already. If they exist,
they are not touched, because they can have any manual customizations.

Some of the customizations that are done to
[path]`discourse_docker/containers/talk.example.org.yml` are these:

* The exposed ports 80 and 443 are commented out, since the container
will get the https requests from `revproxy`.

* The container is connected to the *docker-scripts* network, using
the docker args [opt]`--network` and [opt]`--network-alias`.

* Volumes are mounted to the local directory
[path]`/var/ds/talk.example.org/discourse_docker/` +
(instead of [path]`/var/disocurse/`).
+
NOTE: This is according to the docker-scripts policy of keeping all
the configuration and data of an application in the same directory,
which makes their management easier.

* Email settings are assigned according to the values on
[path]`settings.sh`.

* SSL is enabled (by uncommenting `web.ssl.template.yml`).
+
NOTE: This also requires an SSL certificate. For this reason we are
copying the LetsEncrypt certificate from `revproxy` to the appropriate
place, where it can be loaded by the application as an external SSL
certificate. Actually, a cron job is created that runs the command
[cmd]`ds copy-ssl-cert` periodically, each week.

* We also add a line for each plugin listed on [path]`settings.sh`,
and customize some other settings.

The cron jobs that are created look like this:

* [path]`/etc/cron.d/talk-example-org-restart`
+
[source,bash]
----
# restart the application @/var/ds/talk.example.org each week
0 0 * * 0  root  bash -l -c "ds @/var/ds/talk.example.org restart &> /dev/null"
----

* [path]`/etc/cron.d/talk-example-org-copy-ssl-cert`
+
[source,bash]
----
# copy the ssl cert @/var/ds/talk.example.org each week
0 0 * * 0  root  bash -l -c "ds @/var/ds/talk.example.org copy-ssl-cert &> /dev/null"
----

=== Backup

.[path]`cmd/backup.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/backup.sh[]
----
====

.[path]`cmd/restore.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/restore.sh[]
----
====

Backup and restore use the commands [cmd]`discourse backup` and
[cmd]`discourse restore` inside the app container.

NOTE: During a restore emails are disabled, so the script enables them
again at the end.

=== Maintain

.[path]`cmd/copy-ssl-cert.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/copy-ssl-cert.sh[]
----
====

.[path]`cmd/discourse.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/discourse.sh[]
----

.[cmd]`ds discourse`
[%collapsible]
=====
....
Commands:
  discourse backup                            # Backup a discourse forum
  discourse disable_readonly                  # Disable the readonly mode
  discourse disable_restore                   # Forbid restore operations
  discourse enable_readonly                   # Enable the readonly mode
  discourse enable_restore                    # Allow restore operations
  discourse export                            # Backup a Discourse forum
  discourse export_categories                 # Export categories, all its topics, and all users who posted in those topics
  discourse export_category                   # Export a category, all its topics, and all users who posted in those topics
  discourse export_topics                     # Export topics and all users who posted in that topic. Accepts multiple topi...
  discourse help [COMMAND]                    # Describe available commands or one specific command
  discourse import                            # Restore a Discourse backup
  discourse import_category                   # Import a category, its topics and the users from the output of the export_c...
  discourse import_topics                     # Import topics and their users from the output of the export_topic command
  discourse remap [--global,--regex] FROM TO  # Remap a string sequence across all tables
  discourse request_refresh                   # Ask all clients to refresh the browser
  discourse restore                           # Restore a Discourse backup
  discourse rollback                          # Rollback to the previous working state
....
=====
====

.[path]`cmd/upgrade.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/upgrade.sh[]
----

To upgrade, we simply rebuild the containers with [cmd]`./launcher`. +
But we make a backup before that, just in case.
====

.[path]`cmd/clone.sh`
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/cmd/clone.sh[]
----
====

Cloning is usually needed to build a test site. Sometimes it is
better to test some changes (for example installing a new plugin) on a
clone of the site, and then apply them to the main site.

Cloning is done like this:

. Make a backup of the site.
. Initialize and build a new discourse instance for the clone.
. Restore the backup of the main site to the clone.
. Run discourse commands to rename the domain of the clone.
. Disable any outgoing emails on the clone (since it is used for
testing).

== Installation

. Make sure that xref::install.adoc[docker-scripts] and
xref::revproxy.adoc#_install_revproxy[revproxy] are already
installed. +
You also need a xref::simple-smtp-server.adoc[].

. Prepare {repo}#installation[discourse^]:
+
[source,bash]
----
ds pull discourse
ds init discourse @talk.example.org
cd /var/ds/talk.example.org/
vim settings.sh
----
+
On [path]`settings.sh` set the correct values for [var]`DOMAIN` and
[var]`ADMIN_EMAIL`. Set also the SMTP options.

. Build it with: [cmd]`ds make`

. Check the installation:
+
[%collapsible]
====
[source,bash]
----
cd /var/ds/talk.example.org/
cd discourse_docker/

ls containers/
vim containers/talk.example.org.yml
diff -u \
    samples/standalone.yml \
    containers/talk.example.org.yml \
    > d.diff
vim d.diff

vim containers/talk.example.org-mail-receiver.yml
diff -u \
    samples/mail-receiver.yml \
    containers/talk.example.org-mail-receiver.yml \
    > d.diff
vim d.diff
rm d.diff

docker ps | grep discourse
docker logs talk.example.org
docker logs talk.example.org-mail-receiver
----
====

== Setup

Open https://talk.example.org and continue the setup and configuration
of the site.

* For more details look at:
https://github.com/discourse/discourse/blob/main/docs/INSTALL-cloud.md#8-start-discourse[Start
Discourse^]

* The setup wizard can also be restarted by opening:
https://talk.example.org/wizard

* You should also check the "Admin Guide: Getting Started":
  https://talk.example.org/t/admin-guide-getting-started/

The following sections explain some settings that are important, or
specific to the way that we installed Discourse.

=== Sending emails

==== SMTP settings

The SMTP variables on [path]`settings.sh` are used to set
automatically the values of SMTP settings on the configuration file of
the Discourse container,
[path]`discourse_docker/containers/talk.example.org.yml`, which look
like this:

[source,yaml]
----
env:

  ## TODO: The SMTP mail server used to validate new accounts and send notifications
  # SMTP ADDRESS, username, and password are required
  # WARNING the char '#' in SMTP password can cause problems!
  DISCOURSE_SMTP_ADDRESS: smtp.example.org
  DISCOURSE_SMTP_PORT: 25
  DISCOURSE_SMTP_USER_NAME:
  DISCOURSE_SMTP_PASSWORD:
  DISCOURSE_SMTP_ENABLE_START_TALL: true
  #DISCOURSE_SMTP_DOMAIN: discourse.example.com    # (required by some providers)
  #DISCOURSE_NOTIFICATION_EMAIL: noreply@discourse.example.com    # (address to send notifications from)
----

NOTE: Despite the comment saying that username and password are
required, we actually don't need them because we are using the
xref::simple-smtp-server.adoc[], which allows applications from
trusted hosts to send emails without authentication.

****
[TIP]
--
If you make some modifications to this yaml file, to apply them
you have to rebuild the container:

[source,bash]
----
cd /var/ds/talk.example.org/
cd discourse_docker/
./launcher rebuild talk.example.org
----
--
****

==== Discourse settings

To customize the discourse settings related to sending emails, we
login as admin and go to "Admin / All Site Settings":
https://talk.example.org/admin/site_settings/

There are too many settings there, but we can locate a setting quickly
by typing its name on the filter box. Let's make sure that we set
properly these settings:

* *notification email*: `talk@example.org`
* *contact email*: `admin@example.org`
* *log mail processing failures*: checked
* *unsubscribe via email footer*: checked

For other email settings, that you might want to customize, look at
https://talk.example.org/admin/site_settings/category/email

==== Test sending emails

You can send a test email, from Discourse to your email address, at
https://talk.example.org/admin/email

image::discourse/test-sending-emails.png[]

To check the health of the mail server:

. Go to https://www.mail-tester.com/ and copy the displayed email
address.

. Go to the email settings of Discourse and send a test email to the
address given by the mail-tester.

. Go back to the mail-tester and check the score.

****
.You should see something like this:
[%collapsible]
====
image::discourse/test-email-score.png[]
====
****

=== Mail receiver

Discourse can send emails about the new topics and replies,
notifications, etc. It would be nice if people can also reply or
create new topics by email. This would make Discourse more similar to
a mailing list. This documentation page explains how this is usually
done:
https://meta.discourse.org/t/configure-direct-delivery-incoming-email-for-self-hosted-sites-with-mail-receiver/49487[Configure
incoming email with Mail-Receiver^]

It is using an additional container for receiving and processing
incoming emails. The command [cmd]`ds make` has already created the
container `talk.example.org-mail-receiver`, which is based on the
configuration [path]`containers/talk.example.org-mail-receiver.yml`.
This container is able to receive emails, process them accordingly,
and push the necessary data to the container `talk.example.org`,
using the API of discourse. However some additional steps are needed,
in order to make it functional.

==== Add an MX record on the DNS

On [path]`containers/talk.example.org-mail-receiver.yml` we have set
[opt]`MAIL_DOMAIN` to `talk.example.org`, so we need a DNS record like
this:

....
talk.example.org.         IN    MX    10    smtp.example.org.
....

NOTE: *`smtp.example.org`* is the name of the
xref::simple-smtp-server.adoc[SMTP Server,window=_blank].

Without this DNS record, emails sent to an address `@talk.example.org`
cannot be routed to our server.

We can check that the record is already available with: [cmd]`dig MX
talk.example.org`

==== Relay emails to `mail-receiver`

The container `talk.example.org-mail-receiver` is hosted on the same
server as `smtp.example.org`, so we cannot forward the SMTP ports to
it, because they are being used for the mail server
`smtp.example.org`. However, the simple SMTP server
(`smtp.example.org`) can relay the mails for the domain
`talk.example.org`. The section
xref::simple-smtp-server.adoc#_relay_domains[Relay
domains,window=_blank] describes in details how this can be done.

. Create a local command [cmd]`ds relay-setup`, like this:
+
[source,bash]
----
cd /var/ds/smtp.example.org/
mkdir -p cmd
cat << __EOF__ > cmd/relay-setup.sh
cmd_relay-setup() {
    # create a config file for relay_domains
    cat <<EOF > config/relay_domains
talk.example.org
EOF

    # create a config file for transport_maps
    cat <<EOF > config/transport_maps
talk.example.org    smtp:11.12.13.14:2501
EOF

    # setup transport_maps
    ds exec postconf -e 'transport_maps=hash:/host/config/transport_maps'
    ds exec postmap /host/config/transport_maps

    # setup relay_domains
    ds exec postconf -e relay_domains=/host/config/relay_domains

    # reload postfix configuration
    ds exec postfix reload
}
__EOF__
----
+
It will set the settings [opt]`relay_domains` and
[opt]`transport_maps`. Postfix will accept the emails of the domain
`@talk.example.org`, and will transport (forward) them to the port
*`2501`* of the server *`11.12.13.14`*, using the *`smtp`*
protocol.
+
[NOTE]
====
* *`11.12.13.14`* is the public IP of the host itself
* the port *`2501`* on the host is being forwarded to the docker
container `talk.example.org-mail-receiver`. Check it with:
+
[source,bash]
----
docker ps | grep mail-receiver
----
====

. In order to automatically call the command [cmd]`ds relay-setup`
whenever the SMTP container is rebuilt, we can override the command
[cmd]`ds config` like this:
+
[source,bash]
----
cat <<'EOF' > cmd/config.sh
rename_function cmd_config standard_config
cmd_config() {
    standard_config
    ds relay-setup
}
EOF
----
+
This creates a local `config` command, that extends the standard one.

****
.Using a port different from `2501`
[%collapsible]
====
If for some reason we want to use a port different from `2501`, for
example `2502`, we can do it like this:

. Modify accordingly the script [path]`cmd/relay-setup.sh` and call
it: [cmd]`ds relay-setup`

. Modify the configuration of `mail-receiver` and rebuild it:
+
[source,bash]
----
cd /var/ds/talk.example.org/discourse_docker/
vim containers/talk.example.org-mail-receiver.yml
./launcher rebuild talk.example.org-mail-receiver
----
====
****

==== Set an API key

The mails now should arrive to the `mail-receiver`, but it also needs
an API key in order to push them to the Discourse container.

. Login as admin and go to: https://talk.example.org/admin/api/keys

. Create a new API key:
** *Description:* mail-receiver container
** *User Level:* Single User
** *User:* system
** *Scope:* Granular
** *Scopes:* "email" / "receive emails"

. Copy the generated API key, and set it to the variable
[var]`DISCOURSE_API_KEY` at
[path]`containers/talk.example.org-mail-receiver.yml`

. Rebuild the container `talk.example.org-mail-receiver`:
+
[source,bash]
----
./launcher rebuild talk.example.org-mail-receiver
----

==== Discourse configuration

Now that email is being fed into Discourse, it’s time to explain to
Discourse what to do with the emails it receives.

* Login as admin and navigate to "Admin / All Site Settings / Email":
https://talk.example.com/admin/site_settings/category/email

* Change the following settings:
** Check the setting "*manual polling enabled*".
** In the "*reply by email address*" field, enter:
+
....
replies+%{reply_key}@talk.example.org
....
** Check the "*reply by email enabled*" setting.
** Check the setting "*email in*".

* Other optional settings:
** "*email in allowed groups*"
** "*unsubscribe via email footer*"

You can use any address `@talk.example.org` as an address for category
or group emails. This address has to be defined on the settings of the
category or group.


==== Troubleshooting

. From outside the server, send a test email to
`nobody@talk.example.org`:
+
[source,bash]
----
apt install --yes swaks
swaks --from user@gmail.com --to nobody@talk.example.org
----
+
Alternatively, you can also send it from GMail or any other account.

. Check the logs on the SMTP server, to make sure that it arrived
there:
+
[source,bash]
----
cd /var/ds/smtp.example.org/
ds exec tail /var/log/mail.log -f
----

. Check the logs of `mail-receiver` to make sure that the message
arrived there:
+
[source,bash]
----
docker logs talk.example.org-mail-receiver -f
----
+
You should see a message like this:
+
....
NOQUEUE: reject: RCPT from smtp.example.org[11.12.13.14]: 554 5.7.1 \
    <nobody@talk.example.org>: Recipient address rejected: \
    Mail to this address is not accepted. Check the address and try to send again?;
....


=== Import mailman3 archives

This is useful for communities that are migrating from Mailman3
mailing lists to Discourse. All the past conversations that are
archived on the mailing lists can be imported on Discourse.

****
The documentation page that describes how to do this is:
https://meta.discourse.org/t/migrate-a-mailing-list-to-discourse-mbox-listserv-google-groups-etc/79773[Migrate
a mailing list to Discourse^]
****

The basic steps are these:

. Download the archives for each mailing list from the Hyperkitty
web UI. Upload them to the server.
+
NOTE: If a mailing list is private, you have to login first.

. Before the import, enable these two settings on Discourse:
** "*download remote images to local*"
** "*disable edit notifications*"

. A docker container will be used to do the import. Let's prepare it:
+
[source,bash]
----
cd /var/ds/talk.example.org/discourse_docker/
cp containers/talk.example.org.yml \
   containers/talk.example.org-import.yml
----

. Edit [path]`containers/talk.example.org-import.yml` and add to it
the template `"templates/import/mbox.template.yml"`.
+

.The list of templates should look like this:
[%collapsible]
====
[source,yaml]
----
templates:
  - "templates/postgres.template.yml"
  - "templates/redis.template.yml"
  - "templates/web.template.yml"
  ## Uncomment the next line to enable the IPv6 listener
  #- "templates/web.ipv6.template.yml"
  - "templates/web.ratelimited.template.yml"
  ## Uncomment these two lines if you wish to add Lets Encrypt (https)
  - "templates/web.ssl.template.yml"
  #- "templates/web.letsencrypt.ssl.template.yml"
  - "templates/import/mbox.template.yml"
----
====

. Build and start the _import_ container:
+
[source,bash]
----
cd /var/ds/talk.example.org/
ds backup

cd discourse_docker/
./lancher stop talk.example.org
./lancher rebuild talk.example.org-import
----
+
****
IMPORTANT: Make sure to stop the container `talk.example.org`, before
starting the *import* container. They use the same database, and if
they run simultaneously, the database may be corrupted, and it is
difficult to fix. For this reason, and just in case, make a backup
before starting the import.
****

. Building the container creates an import directory that looks like
this:
+
....
./shared/standalone/import
├── data
└── settings.yml
....
+
We can configure the importer by editing the file
[path]`settings.yml`

. Each subdirectory of [path]`./shared/standalone/import/data/` gets
imported as its own category and each directory should contain the
email archives we want to import. The file names of those do not
matter. For example this directory structure will create two
categories named `private` and `public` (or will import the messages
into categories named like these, if they already exist):
+
....
./shared/standalone/import
├── data
│   ├── private
│   │   ├── foo
│   │   ├── bar
│   ├── public
│   │   ├── 2017-12.mbox
│   │   ├── 2018-01.mbox
└── settings.yml
....

. Let’s start the import by entering the Docker container and
launching the import script inside the Docker container:
+
[source,bash]
----
./launcher enter talk.example.org-import
import_mbox.sh    # inside the Docker container
----

. Stop the import container and start the discourse:
+
[source,bash]
----
./launcher stop talk.example.org-import
./launcher start talk.example.org
----
+
****
IMPORTANT: Make sure that the import container and the application
container do not run simultaneously, because they use the same
database and may corrupt it.
****
+
Discourse will start and _Sidekiq_ will begin post-processing all the
imported posts. You can watch the progress by logging in as admin and
visiting: https://talk.example.com/sidekiq

. If we are satisfied with the result of the import, we can cleanup
the import container:
+
[source,bash]
----
./launcher destroy talk.example.org-import
rm ./containers/talk.example.org-import.yml
rm -R ./shared/standalone/import/
----
+
Otherwise, restore the backup, modify the import data and
the configuration file, and try again.

. Restore the Discourse settings "*download remote images to local*"
and "*disable edit notifications*" to their original values.

****
For more details visit:
https://meta.discourse.org/t/migrate-a-mailing-list-to-discourse-mbox-listserv-google-groups-etc/79773[window=_blank]
****

=== Install plugins

On the configuration of the application
([path]`containers/talk.examples.org.yml`) there is a list of commands
that clone plugin repos. Append the commands for the plugin(s) you
want to install. Afterwards rebuild the container:

[source,bash]
----
./launcher rebuild talk.example.org
----

To remove a plugin, remove it from the list and then rebuild the
container.

TIP: It is a good idea to make a backup before installing a plugin.

== Maintenance

* Backup and restore:
+
[source,bash]
----
cd /var/ds/talk.example.org/

ds backup
ds restore

ds discourse
----

* Upgrade:
+
[source,bash]
----
ds upgrade
----

* Clone:
+
[source,bash]
----
ds clone talk-1.example.org
----
+
****
.Cloning is usually needed to build a test site
[%collapsible]
====
Sometimes it is better to test some changes (for example installing a
new plugin) on a clone of the site, and then apply them to the main
site.

Cloning is done like this:

. Make a backup of the site.
. Initialize and build a new discourse instance for the clone.
. Restore the backup of the main site to the clone.
. Run discourse commands to rename the domain of the clone.
. Disable any outgoing emails on the clone (since it is used for
testing).
====
****

== Misc

A useful admin guide is at:
https://talk.example.org/t/admin-guide-getting-started/6 (you have to
be logged in as admin).

Other docs/pages that might be interesting:

* https://meta.discourse.org/t/create-a-poll-that-others-can-vote-on/77548
* https://www.discourse.org/plugins/calendar.html
* https://meta.discourse.org/t/create-calendars/283773
* https://meta.discourse.org/t/discourse-calendar-and-event/97376
* https://meta.discourse.org/t/create-a-wiki-post/30802
* https://meta.discourse.org/t/using-discourse-as-ticket-system-so-no-balls-are-dropped-woman-juggling/82698[Using discourse as a ticket system]
