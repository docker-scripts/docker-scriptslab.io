= 5. Build a second container
:sectnums:

== Initialize the directory

The test directory so far looks like this:

[subs="+quotes"]
....
[prompt]*/var/test/app1 #* [in]*tree /var/test*
/var/test
├── app1
│   ├── cmd
│   │   └── config.sh
│   ├── logs
│   └── settings.sh
└── scripts1
    ├── Dockerfile
    └── settings.sh
....

To build another container based on `scripts1` you should first
initialize a directory for it, like this:

[subs="+quotes"]
....
[prompt]*/var/test/app1 #* [in]*ds init ../scripts1 @../app2*
Container initialized on '/var/test/app2/'.
[prompt]*/var/test/app1 #* [in]*tree /var/test*
/var/test
├── app1
│   ├── cmd
│   │   └── config.sh
│   ├── logs
│   └── settings.sh
├── app2
│   └── settings.sh
└── scripts1
    ├── Dockerfile
    └── settings.sh
....

****
To understand the strange syntax of this command have a look at its
help:

[subs="+quotes"]
....
[prompt]*/var/test/app1 #* [in]*ds init*    # without arguments
Usage:
    init <app> [@<container>]
        Initialize a container directory by getting the file 'settings.sh'
        from the given app directory.

        The argument <app> can be a subdirectory of '/opt/docker-scripts'
        (where the docker-scripts are usually stored), or an absolute/relative
        path that contains the scripts of the app.

        If the second argument is missing, the current directory will be used
        for initializing the container. If <container> starts with './' or '../'
        it will be relative to the current directory. If <container> starts
        with '/', it will be an absolute path.
        Otherwise, the directory '/var/ds/<container>' will be used.
....

Normally, the scripts are placed under the directory
[path]`/opt/docker-scripts/` and the containers under the directory
[path]`/var/ds/`. Relative or absolute directories are used rarely,
for example for testing.
****

== Customize settings

The file [path]`/var/test/app2/settings.sh` has a content like this:

[source,bash]
----
APP=/var/test/scripts1
IMAGE=scripts1
CONTAINER=app2
----

The name of the directory has been set as the name of the `CONTAINER`,
but you can change it as long as it is unique among all the docker
containers. The name of the `IMAGE` can also be changed, but usually
the name of the scripts directory is a good setting. However you
should not change the value of `APP`, unless you move the scripts
directory to another location.


== Build the container

To build the container try:

[subs="+quotes"]
....
[prompt]*/var/test/app1 #* [in]*ds @../app2 info*

SETTINGS:
    APPDIR:    /var/test/scripts1
    IMAGE:     scripts1
    CONTAINER: app2
    PORTS:     

IMAGE:
    Name:      scripts1:latest
    Created:   3 days ago
    Size:      330MB

[prompt]*/var/test/app1 #* [in]*ds @/var/test/app2 make*
[ . . . . . . . . . . ]

[prompt]*/var/test/app1 #* [in]*cd ../app2*
[prompt]*/var/test/app2 #* [in]*ds info*

SETTINGS:
    APPDIR:    /var/test/scripts1
    IMAGE:     scripts1
    CONTAINER: app2
    PORTS:     

IMAGE:
    Name:      scripts1:latest
    Created:   3 days ago
    Size:      330MB

CONTAINER:
    Name:      app2
    Created:   2 minutes ago
    Status:    Up 2 minutes

[prompt]*/var/test/app2 #* [in]*ds shell*
....

****
If the first argument of [cmd]`ds` starts with `*'@'*`, it switches to
the specified container directory before interpreting the rest of the
command. However it is usually more convenient to [cmd]`cd` to the
container's directory, before giving any [cmd]`ds` commands related
to it.
****

== Fix the configuration

If you run [cmd]`ds shell` you will notice that the prompt is the
standard (not-customized) one. To customize the prompt we can do the
same thing that we did for the first container:

[subs="+quotes"]
....
[prompt]*/var/test/app2 #* [in]*mkdir cmd*
[prompt]*/var/test/app2 #* [in]*cp ../app1/cmd/config.sh cmd/*
[prompt]*/var/test/app2 #* [in]*ds make*
[prompt]*/var/test/app2 #* [in]*ds shell*
....

Notice however that we are making the same `config` customization for
both containers. If we need to make the same customization for all the
containers of this type (containers that are based on `scripts1`), it
is better to make this customization to the scripts:

[subs="+quotes"]
....
[prompt]*/var/test/app2 #* [in]*cd /var/test/scripts1*
[prompt]*/var/test/scripts1 #* [in]*mkdir cmd*
[prompt]*/var/test/scripts1 #* [in]*cp ../app1/cmd/config.sh cmd/*
[prompt]*/var/test/scripts1 #* [in]*rm -rf ../app1/cmd/*
[prompt]*/var/test/scripts1 #* [in]*rm -rf ../app2/cmd/*
[prompt]*/var/test/scripts1 #* [in]*tree*
.
├── cmd
│   └── config.sh
├── Dockerfile
└── settings.sh
[prompt]*/var/test/scripts1 #* [in]*cat cmd/config.sh*
cmd_config() {
  ds inject ubuntu-fixes.sh
}
....

Let's check that it works

[subs="+quotes"]
....
[prompt]*/var/test/scripts1 #* [in]*ds @../app1 make*
[prompt]*/var/test/scripts1 #* [in]*ds @../app1 shell*
[prompt]*/var/test/scripts1 #* [in]*ds @../app2 make*
[prompt]*/var/test/scripts1 #* [in]*ds @../app2 shell*
....

