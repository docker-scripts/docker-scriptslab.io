= Installation

. First make sure that the dependencies are installed:
+
[source,bash]
apt install git make m4 highlight tree

. Then get the code from GitLab:
+
[source,bash]
git clone \
    https://gitlab.com/docker-scripts/ds \
    /opt/docker-scripts/ds

. Then install it:
+
[source,bash]
cd /opt/docker-scripts/ds/
make install

. Finally check that it works:
+
[source,bash]
ds
ds -h
man ds

