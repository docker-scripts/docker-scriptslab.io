= redis
:sectnums:
:repo: https://gitlab.com/docker-scripts/redis
:repo-blob: {repo}/-/blob/master
:repo-raw: {repo}/-/raw/master

This is a helper (auxiliary) container, that may be used by other
applications to improve caching efficiency.

== The scripts

The scripts are located at: {repo} +
We can get them with [cmd]`ds pull`:

[subs="+quotes"]
....
[prompt]*root@vps ~ #* [in]*ds pull redis*
[prompt]*root@vps ~ #* [in]*tree /opt/docker-scripts/redis/*
/opt/docker-scripts/redis/
├── cmd
│   ├── config.sh
│   └── create.sh
├── Dockerfile
├── inject
│   └── redis.sh
├── LICENSE
├── README.md
└── settings.sh
....

Let's look at them more closely.

=== [path]`Dockerfile`

----
include::{repo-raw}/Dockerfile[]
----

It is just including the base
https://gitlab.com/docker-scripts/ds/-/blob/v3.0/src/dockerfiles/jammy[dockerfiles/jammy^]
from docker-scripts.

=== [path]`settings.sh`

[source,bash]
----
include::{repo-raw}/settings.sh[]
----

The comments explain the purpose and usage of the settings. Usually
this container is used in a virtual Docker LAN, in order to serve as a
cache server for other containers on the same LAN. In this case we
don't need to forward any ports and the setting `PORTS` should be
commented. However, if we want to access it from outside (for example
from internet), we should uncomment `PORTS`, which enables port
forwarding for `6379`. In this case we should also make sure to
uncomment the `PASS` setting and to set a strong password to it.

=== [path]`cmd/create.sh`

[source,bash]
----
include::{repo-raw}/cmd/create.sh[]
----

The default `create` command is extended by creating the directories
[path]`conf` and [path]`logs` on the host and mounting to them the
directories [path]`/etc/redis` and [path]`/var/log/redis` from the
container.

=== [path]`cmd/config.sh`

[source,bash]
----
include::{repo-raw}/cmd/config.sh[]
----

=== [path]`inject/redis.sh`

[source,bash]
----
include::{repo-raw}/inject/redis.sh[]
----

We source [path]`settings.sh` because we need to get the variable
`PASS`, which we use below. If it is defined, then we change the
configuration file to require a password. Otherwise, redis can be
accessed without a password.

== Make a container

[source,bash]
----
ds init redis @redis
cd /var/ds/redis/
----

Customize [path]`settings.sh`, for example change the password, and
then run:

[source,bash]
----
ds make

ls conf/
tail logs/redis-server.log
----

== Test it

[subs="+quotes"]
....
[prompt]*root@vps /var/ds/redis #* [in]*ds shell*

[prompt]*redis root@redis:/host
==> #* [in]*redis-cli*
127.0.0.1:6379> [in]*flushall*
(error) NOAUTH Authentication required.
127.0.0.1:6379> [in]*quit*

[prompt]*redis root@redis:/host
==> #* [in]*redis-cli -a thavi5Lah6shiet1gahSui2reiD5ooje*
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
127.0.0.1:6379> [in]*flushall*
OK
127.0.0.1:6379> [in]*set key1 test1*
OK
127.0.0.1:6379> [in]*get key1*
"test1"
127.0.0.1:6379> [in]*monitor*
OK
^C

[prompt]*redis root@redis:/host
==> #* [in]*exit*
exit
[prompt]*root@vps /var/ds/redis #* 
....
