= mariadb
:sectnums:
:repo: https://gitlab.com/docker-scripts/mariadb
:repo-blob: {repo}/-/blob/master
:repo-raw: {repo}/-/raw/master

This container may be used by applications that need a MySQL/MariaDB database.

== The scripts

The scripts are located at: {repo} +
We can get them with [cmd]`ds pull`:

[subs="+quotes"]
....
[prompt]*root@vps ~ #* [in]*ds pull mariadb*
[prompt]*root@vps ~ #* [in]*tree /opt/docker-scripts/mariadb/*
/opt/docker-scripts/mariadb/
├── cmd
│   ├── clone.sh
│   ├── config.sh
│   ├── create.sh
│   └── mariadb.sh
├── Dockerfile
├── inject
│   ├── backup.sh
│   ├── mariadb.sh
│   └── restore.sh
├── LICENSE
├── README.md
└── settings.sh
....

Let's look at them more closely.

=== [path]`Dockerfile`

[source,dockerfile]
----
include::{repo-raw}/Dockerfile[]
----

Here we install the version *10.6* of MariaDB, which is the default
one that comes with ubuntu-22.04 (jammy). If for some reason we need
some other version, we can uncomment the additional repo.

In the last line we make sure to comment out the `bind-address` on the
config file, otherwise it will listen only to the localhost.

=== [path]`settings.sh`

[source,bash]
----
include::{repo-raw}/settings.sh[]
----

Usually the MariaDB container needs to be accessed from other
containers in the same virtual (docker) LAN. In this case we don't
need to uncomment the `PORTS` setting. However, if we want it to be
accessible from outside, for example from the internet, we should
uncomment it.

=== [path]`cmd/create.sh`

[source,bash]
----
include::{repo-raw}/cmd/create.sh[]
----

It mounts outside the container the configuration
([path]`/etc/mysql/mariadb.conf.d`), data ([path]`/var/lib/mysql`),
logs, etc. So, when we rebuild the container, the configuration and
data will persist (will not be lost).

It also creates the _global_ command [cmd]`ds mariadb`. We will see
more about it later.

=== [path]`cmd/config.sh`

[source,bash]
----
include::{repo-raw}/cmd/config.sh[]
----

=== [path]`inject/mariadb.sh`

Configuration script.

[source,bash]
----
include::{repo-raw}/inject/mariadb.sh[]
----

=== Others scripts

- {repo-blob}/cmd/clone.sh[[path]`cmd/clone.sh`^]
- {repo-blob}/cmd/mariadb.sh[[path]`cmd/mariadb.sh`^]
- {repo-blob}/inject/backup.sh[[path]`inject/backup.sh`^]
- {repo-blob}/inject/restore.sh[[path]`inject/restore.sh`^]

== Make

[source,bash]
----
ds init mariadb @db1
cd /var/ds/db1/
# vim settings.sh
ds make
----

== Usage

This container is normally used by other containers/applications that
are installed in the same docker virtual LAN. They can access it by
the name of the container, because Docker provides an internal DNS
service that resolves the name of the container to the IP of the
container.

To facilitate the management of the database by the other containers,
the _global_ command [cmd]`ds mariadb` is installed. It is called
_global_ because it can be called in the context (directory) of any
other container (not in the directory of the mariadb container, as
usual). It can be thought of as an extension of the basic
functionality of the docker-script framework. It is not installed by
default when DS is installed because it makes sense and is useful only
when a mariadb container is installed.

It is installed by placing the script
{repo-blob}/cmd/mariadb.sh[`cmd/mariadb.sh`^] in the directory
[path]`/root/.ds/cmd/` (where [path]`/root/.ds/` is the default value
of [var]`DSDIR`). This is done by the
{repo-blob}/cmd/create.sh#L10-L12[configuration script^].

This global command assumes (requires) that the container that calls
it has defined (on [path]`settings.sh`) the variables: [var]`DBHOST`,
[var]`DBNAME`, [var]`DBUSER`, [var]`DBPASS`. The variable
[var]`DBHOST` keeps the name of the MariaDB container.

From {repo-blob}/cmd/mariadb.sh#L1-L17[its usage^] we can see that it
can be used to _create_, _dump_ and _drop_ a database, as well as to
run SQL commands and scripts on it:

[source,bash]
----
cmd_mariadb_help() {
    cat <<_EOF
    mariadb [ create | drop | dump | sql | script ]
        Manage the database of the container '$CONTAINER'.
        - create
              create the database '$DBNAME'
        - drop
              drop the database '$DBNAME'
        - dump
              dump the database '$DBNAME'
        - sql <query>
              run the given sql query on '$DBNAME'
        - script <file.sql>
              run the given sql script on '$DBNAME'

_EOF
}
----

== Test

=== Basic testing

Let's check that the service `mariadb` inside the container is
running:

[subs="+quotes"]
....
[prompt]*root@vps /var/ds/db1 #* [in]*ds shell*

[prompt]*db1 root@db1:/host
==> #* [in]*systemctl status mariadb*

[prompt]*db1 root@db1:/host
==> #* [in]*mariadb <<< 'show databases;'*
....

=== Use it from a container

Let's create a test container that needs a DB.

[source,bash]
----
ds pull ubuntu
ds init ubuntu @test1
cd /var/ds/test1/
cat settings.sh
sed -i settings.sh -e '/PORTS/d'
ds make
----

We removed [var]`PORTS` because we don't need to access it from the
internet.

To create and manage a database for this container we can use the
command [cmd]`ds mariadb`:

[subs="+quotes"]
....
[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb*
Error: No DBHOST defined on 'settings.sh'
....

It complains that [var]`DBHOST` is missing from [path]`settings.sh`,
but actually we also need to define as well [var]`DBNAME`,
[var]`DBUSER` and [var]`DBPASS`. Let's do it:

[source,bash]
----
cat <<EOF >> settings.sh
DBHOST='db1'
DBNAME='test1'
DBUSER='test1'
DBPASS='pass1'
EOF

cat settings.sh
----

Here, *`db1`* is the name of the mariadb container that we built
previously. The rest of the variables can be anything, but to be
organized and systematic, I am setting the [var]`DBNAME` and
[var]`DBUSER` to the name of the container (`test1` in this case.)

Let's try it again:

[subs="+quotes"]
....
[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb*
Usage:
    mariadb [ create | drop | dump | sql | script ]
        Manage the database of the container 'test1'.
        - create
              create the database 'test1'
        - drop
              drop the database 'test1'
        - dump
              dump the database 'test1'
        - sql <query>
              run the given sql query on 'test1'
        - script <file.sql>
              run the given sql script on 'test1'

[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb create*

[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb sql "show databases;"*
Database
information_schema
mysql
performance_schema
sys
test1

[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb drop*

[prompt]*root@vps /var/ds/test1 #* [in]*ds mariadb sql "show databases;"*
Database
information_schema
mysql
performance_schema
sys
....

To access the database from inside the container we need to install a
MariaDB client first.

[subs="+quotes"]
....
[prompt]*root@vps /var/ds/test1 #* [in]*ds shell*

[prompt]*test1 root@test1:/host
==> #* [in]*apt install --yes mariadb-client*

[prompt]*test1 root@test1:/host
==> #* [in]*source settings.sh*

[prompt]*test1 root@test1:/host
==> #* [in]*echo $DBHOST*

[prompt]*test1 root@test1:/host
==> #* [in]*mariadb --host=$DBHOST --user=$DBUSER --password=$DBPASS <<< "show databases;"*
....
