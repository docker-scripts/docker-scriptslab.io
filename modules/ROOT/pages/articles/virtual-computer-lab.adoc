= Virtual Computer Lab
:sectnums: true
:toc: true
:toclevels: 3

== About VCLab

https://www.youtube.com/watch?v=Q0r_RTMjePI[Virtual Computer Lab^] is a
desktop server in the cloud that can be accessed from a browser. It
provides remote collaboration for the students of a class and their
teacher.

Each user can share his desktop with one or more other users and they
can work on it collaboratively. The teacher can also use
https://epoptes.org/[Epoptes^] to watch what each of the students is
doing, to broadcast his screen to all of them, to access the desktop
of each of the students for helping them, etc.

This simple and non-technical diagram shows how the VCLab is used:

image::vclab/diagram1.png[]

However it also makes sense to install and use the VCLab in a LAN. This is
shown in the following diagram:

image::vclab/diagram2.png[]

Installation is almost the same in both cases, with very small
differences, and we will see both of them.

This short talk explains some more details and includes a quick demo
about VCLab:
https://www.sfscon.it/talks/virtual-computer-lab/[https://www.sfscon.it/talks/virtual-computer-lab/^]

== Installing in the cloud

Let's assume that we already have a VPS on the cloud, and a domain
like `vclab.example.org` that is pointing to it. This diagram shows
what will be installed in this server:

image::vclab/diagram3.png[]

We need to access the web interface of the **guacamole** container
(**3**), and from it we can access the desktop of the Linux servers
(**4**, **5**) through the RDP protocol. However, we also need a
**reverse proxy** container (**2**) for accessing the web interface of
**guacamole** (in case there are other domains hosted in this
server). The **revproxy** container also facilitates the management of
the LetsEncrypt SSL certificate for the domain `vclab.example.org`.

So, the installation steps will be like this:

1. Install
   https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script[docker^]
   and https://gitlab.com/docker-scripts/ds#installation[docker-scripts^].
2. Install
   https://gitlab.com/docker-scripts/revproxy#installation[revproxy^].
3. Install
   https://gitlab.com/docker-scripts/guacamole#installation[guacamole^].
4. Install https://gitlab.com/docker-scripts/linuxmint[linuxmint^].
5. Setup **guacamole** to access **linuxmint**.
6. Repeat the last two steps for
   https://gitlab.com/docker-scripts/debian-desktop[Debian LXDE^].

=== Install docker

A quick way to install
https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script[docker^]
is this:

[source,bash]
----
apt install --yes curl
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
----

=== Install docker-scripts

https://gitlab.com/docker-scripts/ds[docker-scripts^] is a framework
of bash scripts for dockerized applications. We can install it with
these commands:

[source,bash]
----
apt install --yes \
    make m4 highlight git tree
git clone \
    https://gitlab.com/docker-scripts/ds \
    /opt/docker-scripts/ds
cd /opt/docker-scripts/ds/
make install
ds
ds -h
----

=== Install reverse proxy

We can install the container
https://gitlab.com/docker-scripts/revproxy[revproxy^] like this:

[source,bash]
----
ds pull revproxy
ds init revproxy @revproxy
cd /var/ds/revproxy/
vim settings.sh
ds make
----

Before running [cmd]`ds make`, set your email to [var]`SSL_CERT_EMAIL`
(on [path]`settings.sh`) -- it is needed to get LetsEncrypt SSL
certificates.

=== Install Guacamole

https://guacamole.apache.org/[Guacamole^] is like a gateway that
provides access to the computer(s) of the lab through a web
interface. We can install it like this:

[source,bash]
----
ds pull guacamole
ds init guacamole @vclab.example.org
cd /var/ds/vclab.example.org/
vim settings.sh    <1>
ds make
----

****
<1> Make sure to replace `vclab.example.org` with your domain and also to
set proper values on [path]`settings.sh`. You should change at least
the admin password, otherwise the installation script will not
continue:
+
[source,bash]
----
### Guacamole admin user.
### For security reasons change at least the PASS.
ADMIN="admin"
PASS="pass1234"
----
****

After installation is finished, open https://vclab.example.org in
browser and login as admin. You will notice that there are no
connections yet, and this is normal because we haven't installed any
servers yet.

[cols="a,a",frame=none,grid=none]
|===
| image::vclab/guacamole-1.png[]
| image::vclab/guacamole-2.png[]
|===

=== Install LinuxMint

The docker-scripts for
https://gitlab.com/docker-scripts/linuxmint[linuxmint^] install a
minimal version with MATE desktop (but we will see later how to add
more packages to it):

[source,bash]
----
ds pull linuxmint
ds init linuxmint @mate1
cd /var/ds/mate1/
vim settings.sh
----

On [path]`settings.sh`, uncomment modify [var]`EPOPTES_USERS`,
[var]`ADMIN_USER` and [var]`ADMIN_PASS`.

[source,bash]
----
EPOPTES_USERS="admin user1"

ADMIN_USER="admin"
ADMIN_PASS="pass123"
----

Let's also list some user accounts on [path]`accounts.txt`:

[source,bash]
----
cat <<EOF > accounts.txt
user1:pass1
user2:pass2
user3:pass3
user4:pass4
EOF
----

Finally, we can make the container with [cmd]`ds make`.

=== Configure Guacamole

The admin user can do all the configurations that are needed from the
Guacamole web interface (users, connections, etc.).

[cols="a,a",frame=none,grid=none]
|===
| image::vclab/guacamole-3.png[]
| image::vclab/guacamole-4.png[]
|===

However the docker-scripts of guacamole provide a simple command that
can be useful for doing the configuration easily and quickly (and also
for automating it with a script, if needed).

[source,bash]
----
cd /var/ds/vclab.example.org/
ds guac
----

==== Add connections

[source,bash]
----
ds guac conn add mate1 rdp    <1>
ds guac conn add mate1 ssh
ds guac conn ls
----

<1> In this command, **`mate1`** is the name of the `linuxmint`
container that we created, and **`rdp`** is the type of the
connection.

Now we should see these two connections in the home page of
Guacamole. We can login to any of the accounts that we created on
**`mate1`**, for example: (`user1:pass1`).

[cols="a,a",frame=none,grid=none]
|===
| image::vclab/guacamole-5.png[]
| image::vclab/guacamole-6.png[]
|===

==== Add users

It is not a good idea to allow students to login to Guacamole with the
`admin` user, so let's add another Guacamole user and grant it an RDP
connection to **`mate1`**:

[source,bash]
----
ds guac user add class1 pass123
ds guac user ls

ds guac user connect class1 mate1:rdp
ds guac user ls
ds guac user ls class1
----

All the users of a class can use `class1:pass123` to login to
guacamole, and then their individual username and password to login to
**`mate1`** (for example `user1:pass1`).

[cols="a,a",frame=none,grid=none]
|===
| image::vclab/guacamole-7.png[]
| image::vclab/guacamole-8.png[]
|===

== How to collaborate

=== Share the desktop

Any user can share his desktop with one or more friends. This is a
feature provided by Guacamole. You first press `Ctrl+Alt+Shift`, then
select the menu `Share` on the left panel that is opened, and then
`Watch` or `Collaborate`. Guacamole shows the URL that you can share.

image::vclab/desktop-sharing.png[]

Right click on it and copy the URL, then send it to your friends (via
chat, email, etc.). With the `Watch` link, your friends will be able
to see what you are doing on your desktop, but cannot interfere. With
the `Collaborate` link, they will also be able to work on your
desktop, alongside with you.

This might be useful when a teacher wants to show to his students what
he is doing, or when students have to collaborate on a project.

=== Using Epoptes

https://epoptes.org/[Epoptes^] is a management and monitoring tool for
computer labs. It works well in the VCLab and it is installed by
default.

image::vclab/epoptes.png[]

With Epoptes you can:

* See the students that have logged in the VCLab.
* Watch the desktop of each of them.
* Broadcast your desktop to all the students or some of them.
* Access the desktop of a student in order to help him.
* Lock the desktop of one or more students.
* Logout one or more students.

[NOTE]
====
Only the users that are listed at the variable [var]`EPOPTES_USERS`
(on [path]`settings.sh` of **`mate1`**) will have access to Epoptes:

[source,bash]
----
### Epoptes admins. Uncomment to enable.
EPOPTES_USERS="admin user1"
----
====

== Manage user accounts

=== Create new accounts

We have already seen that user accounts can be listed on the file
[path]`accounts.txt`, before running the command [cmd]`ds make`, and
they will be created automatically. This file looks like this:

[source]
----
user1:pass1
user2:pass2
...
----

New user accounts can also be created with a command like this:

[source,bash]
----
ds users create accounts1.txt
----

Here, the file [path]`accounts1.txt` has the same format as the file
[path]`accounts.txt`.

If we omit the file, the command [cmd]`ds users create` will read
from the standard input (the keyboard). So, we can type
`username:password` and then press Enter. Finally, press `Ctrl-d` to
terminate the input.

We can also send data through a pipe, like this:

[source,bash]
----
echo 'user10:pass10' | ds users create
cat accounts2.txt | ds users create
----

=== Backup user accounts

We can backup and restore user accounts like this:

[source,bash]
----
ds users backup
ds users restore backup/users-20200528.tgz
----

Some other commands of `ds users` are export/import, which are similar
to backup/restore, but save only the username and password of each
account, not the home directory.

****
Users can also be managed by the admin(s) of the linuxmint system,
using `sudo users.sh`. For example:

[source,bash]
----
sudo users.sh create accounts.txt
sudo users.sh backup
sudo users.sh restore /host/backup/users-20200528.tgz
----
****

== Install another server

This time let's install Debian LXDE in a container. It will be
accessed through Guacamole.

[source,bash]
----
ds pull debian-desktop
ds init debian-desktop @lxde1
cd /var/ds/lxde1
vim settings.sh    <1>
ds make
ls
cat accounts.txt   <2>
----

<1> No modification if [path]`settings.sh` is needed in this case.
<2> These example accounts were created automatically, since the file
[path]`accounts.txt` was missing.

The next step is to create a connection in guacamole, and to allow
user `class1` to access this connection:

[source,bash]
----
### go to guacamole
cd /var/ds/vclab.example.org/

### create a new connection
ds guac
ds guac conn add lxde1 rdp
ds guac conn ls

### allow user class1 to access this connection
ds guac user ls
ds guac user connect class1 lxde1:rdp
ds guac user ls class1
----

Now we can login on guacamole as user `class1` and click on the
connection `lxde1:rdp`. On the RDP login window we can use the
credentials `user1:pass1`.

[cols="a,a",frame=none,grid=none]
|===
| image::vclab/guacamole-9.png[]
| image::vclab/lxde1.png[]
|===

Let's also create the same user accounts that are on **`mate1`**:

[source,bash]
----
### export the users of mate1
cd /var/ds/mate1/
ds users export
ds users export > user-accounts.passwd

### move the exported users to lxde1
mv user-accounts.passwd ../lxde1/
cd ../lxde1/

### remove the first two lines
cat user-accounts.passwd
sed -i user-accounts.passwd -e '1,2 d'
cat user-accounts.passwd

### import users from the file
ds exec ls /home/
ds users import user-accounts.passwd
ds exec ls /home/
----


== Installing VCLab in LAN

Installing VCLab in LAN is very similar to installing it in the Cloud,
but in this case we don't have a public IP, we don't have a real
domain name, and we cannot get an SSL certificate. Since we don't have
a domain name and don't need to get an SSL certificate, we also don't
need to use a *revproxy*.

****
Also in this case we need to make sure that the computer where VCLab
is installed has a good network connection to the LAN (for example at
least a 1Gbps network interface), since it might have to serve
simultaneously 20-30 clients.
****

=== Preparation

Let's summarize the initial steps quickly:

. Install *docker*:
+
[source,bash]
----
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
----

. Install *docker-scripts*:
+
[source,bash]
----
apt install --yes \
    make m4 highlight git tree
git clone \
    https://gitlab.com/docker-scripts/ds \
    /opt/docker-scripts/ds
cd /opt/docker-scripts/ds/
make install
----

=== Guacamole

. Install *Guacamole*:
+
--
[source,bash]
----
ds pull guacamole
ds init guacamole @guacamole
cd /var/ds/guacamole/
vim settings.sh    <1>
ds make
----

<1> You should change at least the admin password, same as
before. However, you should also comment out `DOMAIN` and uncomment
`PORTS="443:443"`.  Since the container is not being served by
*revproxy*, we need to forward the port *443* to it.
--

. Setup the configuration of Guacamole:
+
[source,bash]
----
ds guac conn add mate1 rdp    
ds guac conn add mate1 ssh
ds guac user add class1 pass123
ds guac user connect class1 mate1:rdp
----
+
We can setup guacamole with `mate1` even if the container `mate1` is
not installed yet.

=== LinuxMint

. Install *linuxmint*:
+
[source,bash]
----
ds pull linuxmint
ds init linuxmint @mate1
cd /var/ds/mate1/
vim settings.sh
ds make
----

. Create user accounts on **`mate1`**:
+
[source,bash]
----
cat <<EOF >> accounts.txt
user3:pass3
user4:pass4
EOF

ds users create accounts.txt
ds exec ls /home/
----
+
Installation scripts have created automatically the users `user1` and
`user2`, and we just created some more user accounts.

== Customizing VCLab

=== Add admin and epoptes users

- Create an admin user from the command line:
+
[source,bash]
----
ds inject add-admin.sh admin1 pass123
ds exec ls /home/
----
+
****
Different from normal users, an admin user can use [cmd]`sudo` to
install packages and to manage users (create/backup/restore). For
example:

[source,bash]
----
sudo apt install frozen-bubble
sudo apt purge frozen-bubble

echo "user01:pass01" | sudo users.sh create
sudo users.sh backup
----
****

- Allow `user1` to use Epoptes:
+
[source,bash]
----
ds exec adduser user1 epoptes
----
+
****
Basically, this executes the command [cmd]`adduser user1 epoptes`
inside the container, which adds user `user1` to the group
`epoptes`. Only the users of this group are allowed to open Epoptes.
****

[WARNING]
====
These changes are not persistent. This means that if we rebuild the
container (with [cmd]`ds make` or [cmd]`ds remake`) they will be lost
and we will have to do them again.

If we define these users on [path]`settings.sh` instead, they will be
persistent with respect to [path]`ds make` and [path]`ds remake`.
====

=== Install more packages

- As mentioned on the previous section, admin users can manage
packages with [cmd]`sudo apt`, like this:
+
[source,bash]
----
sudo apt install frozen-bubble
sudo apt purge frozen-bubble
----

- From the host system (from outside the container) new packages can
also be installed like this:
+
[source,bash]
----
ds exec apt install frozen-bubble
----

WARNING: These installations are not persistent. When we rebuild the
container (with [cmd]`ds make` or [cmd]`ds remake`) we will have to
install them again.

=== Install packages persistently

To rebuild the container with additional packages, follow these steps:

. First create a file named [path]`packages`, with a content like
this:
+
[source,dockerfile]
----
RUN apt install --yes frozen-bubble
----

. Then change the name of the IMAGE on [path]`settings.sh`:
+
[source,bash]
----
IMAGE="mate1"
----

. Finally build and make the container:
+
[source,bash]
----
ds build && ds make
----
+
This is going to build the image locally (instead of pulling it from
the docker hub). Usually it takes much longer than just pulling an
off-the-shelf image.
+
CAUTION: Don't use [cmd]`ds remake` because it removes first the image
(that was just built with [cmd]`ds build`).

. You can also override the command [cmd]`ds remake` by creating a
local one on [path]`cmd/remake.sh`, like this:
+
[source,bash]
----
mkdir -p cmd
cat << 'EOF' > cmd/remake.sh
cmd_remake() {
    # backup
    ds users backup

    # reinstall
    ds build
    ds make
    ds restart

    # restore
    local datestamp=$(date +%Y%m%d)
    ds users restore backup/users-$datestamp.tgz
}
EOF
----
+
Now, when you run [cmd]`ds remake`, it will run [cmd]`ds build` and
[cmd]`ds make`, taking care to backup first and restore afterwards.

=== Create guest accounts

Guest accounts are shared accounts that are used temporarily by
different users. The home directory and settings of a guest account
are reset automatically when the user logs out (or logs in).

****
.What are the benefits of using guest accounts?
[%collapsible]
====
Guest accounts minimize the need for maintenance; no matter how much
the users mess with the settings, in the end everything would go back
to the default settings automatically. For example a user might change
the settings of the desktop and make it difficult for the other users
to use it. Without guest accounts an administrator would have to fix
it back again.

Guest accounts also ensures the safety of the users, because no open
accounts or passwords are left behind. For example a user may login to
his gmail account and forget to logout (or maybe the internet
connection breaks). Then the next user would be able to read his
emails because the session is open in the browser, unless the account
is wiped clean. Or maybe a user writes a personal document (e.g. a
homework) and forgets to delete it when he is done. Then the next user
would be able to copy it.
====
****

Before creating guest accounts, make sure that these lines are
uncommented on [path]`settings.sh`:

[source,bash]
----
### Guest account. Uncomment to enable.
### This account will be used as a template for guest accounts.
GUEST_USER="guest"
GUEST_PASS="pass123"
----

NOTE: Make sure to change the password too, before running [cmd]`ds
make`.

You can create guest accounts like this:

[source,bash]
----
cd /var/ds/mint1/
ds guest-accounts

# generate a list of guest accounts
ds guest-accounts generate 3
ds guest-accounts gen 30 > guests.txt
tail guests.txt

# create guest accounts on linuxmint
ds guest-accounts create-lm guests.txt
ds exec ls /home/guest-accounts/
----

We have to create as well a login on guacamole for each guest account:

[source,bash]
----
cd /var/ds/vclab.example.org/

# create a new connection with the option '--guest'
ds guac conn add mate1 rdp --guest
ds guac conn ls

# get the list of guest accounts from linuxmint
cp ../mate1/guests.txt .
tail guests.txt

# add a guest login for each line in the file
cat guests.txt \
  | while read line ; \
      do user=$(echo $line | tr ':' ' ') ; \
      echo $user ; \
      ds guac guest add mate1:rdp $user ; \
    done
ds guac guest ls mate1:rdp:guest
----

To customize the appearance of the guest accounts, we can login to the
account specified by [var]`GUEST_USER` and [var]`GUEST_PASS` (on
[path]`settings.sh`) and modify this one. When done with
modifications, open a terminal in this account and save them with:

[source,bash]
----
save-guest-config.sh
----

Now the changes will be reflected to the guest accounts, because this
account is used as a template for them.

