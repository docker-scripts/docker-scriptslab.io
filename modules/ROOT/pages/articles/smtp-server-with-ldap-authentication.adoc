= SMTP server with LDAP authentication
:sectnums:
:toc:
:toclevels: 3

== Introduction

We want a mail server that:

* Allows you to send email if you authenticate with a valid username
and password, which are stored on an LDAP server.

* If someone sends email to one of the mail domains supported by the
server, the recipient address will be checked on the LDAP directory
whether it is valid, before the mail is accepted.

* The received emails are not stored locally, but are forwarded
instead to an external email address that belongs to the user (and is
recorded in the LDAP directory).

These are called "forward-only" or "no-mailbox" email accounts.

This kind of mail server could be useful for small organizations or
companies. It allows them to have branded email addresses and to use
their own mail server for sending emails. However it spares their
users from having multiple email accounts, allowing them to receive
all the emails in a single mailbox, on their preferred mail provider
(for example gmail.com, mailbox.org, proton.me, etc).

At the same time, it relieves the postmaster of the organization from
having to manage mailboxes and backups, scanning emails for viruses
and spam, having to deal with legal issues (like GDPR), etc. These are
not easy tasks and they can be done much more professionally from the
dedicated email providers.

== Requirements

=== Simple SMTP server

This mail server is a kind of extension to the
xref::simple-smtp-server.adoc[window=_blank]. So, first of all, make
sure to follow the instructions and install it.

Once you have it installed, you already have a "forward-only" mail
server. To add a forwarding address (or _alias_):

. Add a line like this to [path]`config/virtual_alias_maps`:
+
....
first.last@example.org  <destination-email-address>
....

. Then update the [path]`virtual_alias_maps.db`:
+
[source,bash]
----
ds exec postmap /host/config/virtual_alias_maps
----
+
NOTE: The command [cmd]`ds inject update.sh` will update it as well,
along with some other things.

****
.Test email forwarding
[%collapsible]
=====
Try to send an email to `##test1##@example.org`:

====
[source,bash,subs="+quotes"]
----
swaks -tlso \
    --from info@example.org \
    --to ##test1@example.org##
----

[subs="+quotes"]
....
...
<** 550 5.1.1 <test1@example.org>:
                      ##Recipient address rejected:##
                      ##User unknown in virtual alias table##
...
....
====

It will fail because the recipient does not exist on the alias
table. On [path]`config/virtual_alias_maps` add a line like this:

[subs="+quotes"]
....
##test1@example.org##  <username>@gmail.com
....

Then update the alias db and verify that now you can send email to
this address:

====
[source,bash]
----
#ds inject update.sh
ds exec postmap /host/config/virtual_alias_maps

swaks -tlso \
    --from info@example.org \
    --to test1@example.org
ds exec tail /var/log/mail.log
----
====
=====
****

=== LDAP server

The default setup of xref::simple-smtp-server.adoc[window=_blank]
allows us to send emails only from the "trusted hosts" (whose IPs are
listed on [path]`config/trusted_hosts`). This prevents any spammers
from abusing the mail server.

If some users need to send emails from anywhere (any random IP), they
should be able to authenticate themselves to the SMTP server, for
example with a username and password. The SMTP server should be able
to check their username and password in a database (which may be an
LDAP directory or a relational DB like MariaDB, PostgreSQL, etc.)

In our case we are going to use an xref::apps/openldap.adoc[OpenLDAP
server^] that stores the needed user details (username, password and
forward email). So, make sure to follow the instructions and
install it.

== Setup

=== Add data to LDAP

Let's populate LDAP with some data. We need app credentials, so that
the mail server is allowed to check the data stored in LDAP. We also
need a couple of user entities, that we can use for testing.

. First, let's make sure that we have the organizational units `apps`
and `users`:
+
[source,bash]
----
ds ou
ds ou add apps
ds ou add users
ds ou ls
ds search "(objectClass=organizationalUnit)"
----
+
OU-s are like branches or subdirectories in LDAP. The unit `apps` is
for the mail server and other applications that need to check the user
data on LDAP. The unit `users` is for the user entities.

. Let's also add a couple of test users:
+
[source,bash]
----
ds user
ds user add \
    users \
    user1@example.org \
    abc@mail.com \
    pass123
ds user add \
    users \
    user2@example.org \
    xyz@mail.com
ds set-passwd \
    pass234 \
    uid=user2@example.org,ou=users,dc=example,dc=org
ds user ls
ds search "(objectClass=inetOrgPerson)"
----
+
****
.User data fields
[%collapsible]
====
The users that are created with [cmd]`ds user add` have a minimal data
structure that looks like this:

....
dn: uid=user1@example.org,ou=users,dc=example,dc=org
uid: user1@example.org
objectClass: top
objectClass: inetOrgPerson
sn: user1@example.org
cn: user1@example.org
mail: abc@mail.com
userPassword:: e1NTSEF9OUU2icTExR3FBNXRnOGduUmxhMmM=
....

This is sufficient for our needs.
====
****
+
NOTE: The users can change or recover their password with a web
interface at https://ldap.example.org/

. Create app credentials for the mail server:
+
[source,bash]
----
ds app
ds app add mail1 pass123
ds app ls
ds search "(objectClass=organizationalRole)"
----

. Let's also check that we are able to read data from the LDAP server,
using the credentials of `cn=mail1,ou=apps,dc=example,dc=org`:
+
--
[source,bash]
----
apt install ldap-utils

ldapsearch -xLLL \
    -H ldap://ldap.example.org/ \
    -D cn=mail1,ou=apps,dc=example,dc=org \  <1>
    -w pass123 \                             <2>
    -b ou=users,dc=example,dc=org \          <3>
    "(uid=user1@example.org)"                <4>
----

<1> This is like a username or an **app ID** (it is called "bind dn").

<2> This is like a password or **secret token** (called "bind
password")

<3> Branch of the directory where the searching starts (called "base
dn" or "search base").

<4> The LDAP filter used for searching.

[source,bash]
----
ldapsearch -xLLL -ZZ \                         <1>
    -H ldap://ldap.example.org/ \
    -D cn=mail1,ou=apps,dc=example,dc=org \
    -w pass123 \
    -b ou=users,dc=example,dc=org \
    "(uid=user1@example.org)" \
    uid mail                                   <2>    
----

<1> The option [opt]`-ZZ` is for using a *TLS* connection.

<2> List of attributes to show.

TIP: If there is any problem, add the option [opt]`-d9` for
debugging.
--

=== Enable LDAP authentication on the SMTP server

We need to uncomment the [opt]`LDAP_` variables on
[path]`settings.sh`, and remake the container.

. Go to the SMTP app directory:
+
[source,bash]
----
cd /var/ds/smtp.example.org/
----

. Edit [path]`settings.sh` and uncomment the LDAP settings, giving
them proper values:
+
[source,bash]
----
LDAP_SERVER_HOST="ldap.example.org"
LDAP_SEARCH_BASE="ou=users,dc=example,dc=org"
LDAP_BIND_DN="cn=mail1,ou=apps,dc=example,dc=org"
LDAP_BIND_PW="pass123"
----

. Re-make the container: [cmd]`ds make`

When the [opt]`LDAP_` settings are available,
https://gitlab.com/docker-scripts/postfix/-/blob/master/inject/setup.sh#L12-15[the
scripts^] will setup some additional configurations that instruct the
SMTP server to lookup on the given LDAP server, or to authenticate
users there, when needed.

== Testing

=== Add users

Test instructions below assume that the test users
(`user1@example.org`, `pass1`) and (`user2@example.org`, `pass2`)
exist on the LDAP database. They can be added like this:

[source,bash]
----
cd /var/ds/ldap.example.org/

ds ou add users

ds user
ds user add users \
    user1@example.org \
    forward-address@mail.com \
    pass1
ds user add users \
    user2@example.org \
    forward-address@mail.com \
    pass2
ds user ls
----

You can use your normal email address instead of
`forward-address@mail.com`.

For a quick automated test of the basic functionality of the server
try: [cmd]`ds test2`.

The manual tests described in the following sections.

=== Test the LDAP queries

[source,bash]
----
cat config/smtpd_sender_login_maps.ldap
cat config/virtual_alias_maps.ldap

ds shell

postconf -f virtual_alias_maps
postconf -f smtpd_sender_login_maps

postmap -q user1@example.org \
    ldap:/host/config/virtual_alias_maps.ldap
postmap -q user1@example.org \
    ldap:/host/config/smtpd_sender_login_maps.ldap
----

If the postmap queries don't return the expected result, append the line
`debuglevel = -1` on `smtpd_sender_login_maps.ldap` and
`virtual_alias_maps.ldap` and try to identify the problem. You can also
try with `ldapsearch`, like this:

[source,bash]
----
### on the host, outside the container
apt install ldap-utils
ldapsearch -x -LLL -ZZ \
    -H ldap://ldap.example.org/ \
    -D cn=mail1,ou=apps,dc=example,dc=org \
    -w pass123 \
    -b ou=users,dc=example,dc=org \
    "(uid=user1@example.org)"
----

=== Test SASL authentication

[source,bash]
----
ds shell
cat /etc/saslauthd.conf
cat /etc/postfix/sasl/smtpd.conf
cat /etc/default/saslauthd | grep MECHANISMS
systemctl status saslauthd
testsaslauthd \
    -f /var/spool/postfix/var/run/saslauthd/mux \
    -u user1 -r example.org -p pass123
postconf smtpd_sasl_auth_enable
----

=== Test sending and forwarding emails

These test commands are supposed to run on a computer other than the
server, which has a public IP that is NOT on the list of trusted hosts
(`config/trusted_hosts`). While running them, it may be useful to check
the logs on the mail server, on a separate tab/terminal:

[source,bash]
----
cd /var/ds/smtp.example.org/
ds shell
tail /var/log/mail.log -f
----

==== Try to send message without authentication

Send a test message from `user1@example.org` to a gmail account:

[source,bash]
----
swaks --server smtp.example.org \
      --from user1@example.org --to user@gmail.com
[. . .]
<** 450 4.3.2 Service currently unavailable
[. . .]

swaks --server smtp.example.org \
      --from user1@example.org --to user@gmail.com
[. . .]
<** 554 5.7.1 <user@gmail.com>: Relay access denied
[. . .]
----

We are trying it twice. The first time you try to send an email from a
new client the service is automatically refused (in order to confuse
spam bots). The second time the connection is accepted, however it
fails because the sender is not authenticated.

TIP: The test above should fail (sending email without
authentication). If it succeeds, make sure that your public IP is not
on `config/trusted_hosts`. If you modify this file you should also run
`ds inject update.sh`.

==== Send a message with authentication

[source,bash]
----
swaks --server smtp.example.org \
      --from user1@example.org \
      --to user@gmail.com \
      --auth-user user1@example.org \
      --auth-password pass123
[. . .]
<-  235 2.7.0 Authentication successful
[. . .]
----

This time it should succeed, if the password is correct.

==== Send a message from the wrong address

Let's try to send a message from the same user as before, but with
`--from user2@example.org`:

[source,bash]
----
swaks --server smtp.example.org \
      --auth-user user1@example.org \
      --auth-password pass123 \
      --from user2@example.org \
      --to user@gmail.com
[. . .]
<-  235 2.7.0 Authentication successful
[. . .]
<** 553 5.7.1 <user2@example.org>: Sender address rejected: 
                         not owned by user user1@example.org
[. . .]
----

This `--from` address does not belong to the authenticated user, so the
server refuses to send the mail.

==== Test forwarding emails

Let's send an email from `user1` to `user2`. It should be forwarded to
the external email address of `user2`:

[source,bash]
----
swaks --server smtp.example.org \
      --auth-user user1@example.org \
      --auth-password pass123 \
      --from user1@example.org \
      --to user2@example.org \
----

Try also to send an email from an external email address to
`user1@example.org` and `user2@example.org`.

== Using

=== Create account

The forward-only email address assumes that you already have a
*primary* email account (on the provider of your choice) where the
incoming emails are forwarded. We can create it like this:

. Add a new user entity on the LDAP server:
+
[source,bash]
----
cd /var/ds/ldap.example.org/
ds user add users \
    firstname.lastname@example.org \
    <primary-email-address> \
    pass1234
----

. Go to https://ldap.example.org/ and make sure that you can reset the
password. Users can either change the password, if they know the
current one (`pass1234`), or reset the password with the help of the
`<primary-email-address>`.

=== Receive messages

All the emails that are sent to your forward-only email address
(`firstname.lastname@example.org`) will end up in the inbox of your
*primary* email account (`<primary-email-address>`).

TIP: To be more organized, you can use filtering to automatically move
all the emails that come to `firstname.lastname@example.org` to their
own folder.

=== Send messages

So far so good, but what if you want to send emails from your
forward-only email address, or to reply to an incoming message?

The answer lies on the SMTP settings of your mail client. Ideally, you
should use a mail client that supports multiple SMTP servers, or at
least multiple email accounts (one for the primary address, and one for
the forward-only one). Thunderbird, for example, is a suitable email
client.

The setup on Thunderbird would be like this:

. First of all, make sure to add in Thunderbird an account for the
  *primary* email address. +
  (Probably you already have it.)

. Add also an additional SMTP server, with settings like this:

** *Description:* `firstname.lastname@example.org` (it can actually be
   anything).
** *Server Name:* `smtp.example.org`
** *Port:* `587`
** *Connection security:* `STARTTLS`
** *Authentication method:* `Normal password`
** *User Name:* `firstname.lastname@example.org`


. On the settings of the *primary* account, add an additional
  *identity*, with the forward-only email address
  (`firstname.lastname@example.org`).

. Make sure that the "Sending Mail Server SMTP" for this new identity
  is the SMTP server created above.

. On the settings of this new identity, also make sure that the sent
  emails are saved on the *local* "Sent" directory, and archived
  emails on the *local* "Archive" directory. Do the same for "Drafts"
  and "Templates" +
  (Probably you have to create these local directories first.)

. When you send a new email (or reply to an email) switch the identity
  to the one for `firstname.lastname@example.org`.


.This https://peertube.debian.social/w/rq7cL7tRE6aT9R5tVzyDmB[video^] shows how to do it
[%collapsible]
====
.How to add a new identity on Thunderbird
video::smtp-server-with-ldap-authentication/add-new-identity-on-thunderbird.m4v[]
====

== References

* https://www.vennedey.net/resources/2-LDAP-managed-mail-server-with-Postfix-and-Dovecot-for-multiple-domains[window=_blank]
