= Create an Incus container
:sectnums: true
:repo: https://gitlab.com/docker-scripts/misc
:repo-raw: {repo}/-/raw/main

== Create the container

Let's create an ubuntu container, named `##test1##`.

[source,bash,subs="+quotes"]
----
incus launch images:ubuntu/noble ##test1## \
    -c security.nesting=true \
    -c security.syscalls.intercept.mknod=true \
    -c security.syscalls.intercept.setxattr=true

incus list
incus list -c ns4t
incus info ##test1##
incus config show ##test1##
----

The configuration [opt]`-c security.nesting=true` is needed in order
to run docker inside the container. However if the container is
unprivileged, it does not really have any security implications.

Similarly, the other two configuration options are needed so that
docker can handle images efficiently.

NOTE: For a Debian container use a debian image, for example
`images:debian/12`. The rest of the instructions will be almost the
same.

== Customize the container

Let's make a few small improvements inside the container.

* Get a shell in the container:
+
[source,bash]
----
incus shell test1
----

* Update/upgrade:
+
[source,bash]
----
apt update
apt upgrade --yes
----

* Uncomment some aliases (in debian):
+
[source,bash]
----
sed -i ~/.bashrc \
    -e 's/# export LS_OPTIONS=/export LS_OPTIONS=/' \
    -e 's/# alias ls=/alias ls=/' \
    -e 's/# alias ll=/alias ll=/' \
    -e 's/# alias l=/alias l=/'
cat ~/.bashrc
----

* Set a better prompt:
+
[source,bash]
----
sed -i ~/.bashrc -e '/bashrc_custom/d'
echo 'source ~/.bashrc_custom' >> ~/.bashrc
----
+
[source,bash,subs="+quotes"]
----
[cmd]*cat <<'EOF' > ~/.bashrc_custom*
# set a better prompt
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
[cmd]*EOF*
----

* Make sure that bash-completion is enabled:
+
[source,bash,subs="+quotes"]
----
apt install --yes bash-completion

[cmd]*cat <<'EOF' >> ~/.bashrc_custom*
# enable programmable completion features
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    source /etc/bash_completion
fi
[cmd]*EOF*
----
+
[source,bash]
----
source ~/.bashrc
----

* Make sure that [cmd]`vim` is installed and enable its *dark
  background* setting:
+
[source,bash]
----
apt install --yes vim
sed -i /etc/vim/vimrc \
    -e 's/^"set background=dark/set background=dark/'
----

* Enable automatic security updates:
+
[source,bash]
----
apt install --yes unattended-upgrades
----

== Set a fixed IP

Most of the containers need to have a fixed IP, so that ports can be
forwarded to them from the host, etc. It is done differently on Debian
and Ubuntu.

=== In Debian

[source,bash,subs="+quotes"]
----
IP=10.25.177.206/24
GW=10.25.177.1

[cmd]*cat <<EOF* > [path]*/etc/systemd/network/eth0.network*
[Match]
Name=eth0

[Address]
Address=$IP

[Route]
Gateway=$GW

[Network]
DHCP=no
DNS=8.8.8.8
[cmd]*EOF*

cat /etc/systemd/network/eth0.network
systemctl restart systemd-networkd

ip addr
ip ro
ping google.com
----


=== In Ubuntu

[source,bash,subs="+quotes"]
----
apt purge cloud-init
rm /etc/netplan/50-cloud-init.yaml

IP=10.25.177.206/24
GW=10.25.177.1
[cmd]*cat <<EOF* > [path]*/etc/netplan/01-netcfg.yaml*
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: no
      addresses:
        - $IP
      nameservers:
        addresses: [8.8.8.8, 8.8.4.4]
      routes:
        - to: default
          via: $GW
[cmd]*EOF*

chmod 600 /etc/netplan/01-netcfg.yaml
cat /etc/netplan/01-netcfg.yaml

netplan apply

ip address
ip route
ping 8.8.8.8
----

== Install Docker

Not all the containers need to have Docker installed, but most of them do.

include::ROOT:howto/install-docker.adoc[lines=2..-1]


== Install docker-scripts

Not all the containers need to have [app]`docker-scripts` installed.

include::../install.adoc[lines=2..-1]

== Script

We can use a script to automate the steps above.

****
.Script: {repo-raw}/incus/provide-container.sh?inline=false[[path]`incus/provide-container.sh`]
[%collapsible]
====
[source,bash]
----
include::{repo-raw}/incus/provide-container.sh[]
----
====
****
