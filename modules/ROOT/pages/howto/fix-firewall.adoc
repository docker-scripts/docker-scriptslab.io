= Fixing the firewall
:sectnums:

== The problem

If we create a test container, we will notice that the network in the
container is not working:

[source,bash]
----
incus launch images:ubuntu/22.04 u22
incus ls
incus exec u22 -- ip addr
----

The container did not get an IP, as it normally should.

However, if you stop *firewalld* and restart the container, everything
works fine.

[source,bash]
----
systemctl status firewalld
systemctl stop firewalld

incus restart u22

incus ls
incus exec u22 -- ip addr
incus exec u22 -- ping 8.8.8.8

systemctl start firewalld
systemctl status firewalld
----

****
By the way, IP forwarding should already be enabled in the kernel of
the host:

[source,bash]
----
sysctl net.ipv4.ip_forward
cat /proc/sys/net/ipv4/ip_forward
----

If it is not, enable it like this:

[source,bash]
----
echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf
sysctl -p
----
****

So the problem is that the firewall is not configured properly. Let's
fix it.

== Add the bridge interface to the trusted zone

Any interface that is not explicitly added to a zone, is added to the
default zone, which is the zone `public`. This zone is meant for the
interfaces that are facing the public internet, so it is
restricted. For example DHCP requests are blocked, and the containers
cannot get an IP.

To fix this, we can add the bridge interface to the `trusted` zone,
where everything is allowed:

[source,bash]
----
firewall-cmd --zone=trusted --list-all
firewall-cmd --permanent --zone=trusted --add-interface=incusbr0
firewall-cmd --reload
firewall-cmd --zone=trusted --list-all
----

Let's check that it is working:

[source,bash]
----
incus restart u22
incus ls
incus exec u22 -- ip addr
incus exec u22 -- ping 8.8.8.8
----

== Fix the FORWARD chain

If the ping is still not working, usually the problem is that
forwarding is blocked. If you try [in]`iptables-save | head` and see
something like this: `:FORWARD DROP [4:2508]`, it means that the
policy for the *FORWARD* chain is *DROP*. Maybe it is set by Docker,
if you have installed it.

You can make the default policy *ACCEPT*, like this: [in]`iptables -P
FORWARD ACCEPT`.  However, the next time that the server will be
rebooted, or [app]`firewalld` restarted, you may loose this
configuration.

A better way is to add a direct (explicit) rule with
[cmd]`firewall-cmd`, like this:

[source,bash]
----
firewall-cmd --permanent --direct --add-rule \
    ipv4 filter FORWARD 0 -j ACCEPT
firewall-cmd --reload

firewall-cmd --direct --get-all-rules
----

This will enable (ACCEPT) forwarding for all the interfaces, the
current ones and the ones that will be created in the future. If this
is not what you want, you can use more specific rules, like these:

[source,bash,subs="+quotes"]
----
firewall-cmd --permanent --direct --remove-rule \
    ipv4 filter FORWARD 0 -j ACCEPT

firewall-cmd --permanent --direct --add-rule \
    ipv4 filter *-i incusbr0* FORWARD 0 -j ACCEPT
firewall-cmd --permanent --direct --add-rule \
    ipv4 filter *-o incusbr0* FORWARD 0 -j ACCEPT

firewall-cmd --reload
firewall-cmd --direct --get-all-rules
----

== Cleanup

Let's test again and then remove the test container:

[source,bash]
----
incus exec u22 -- ping 8.8.8.8

incus stop u22
incus rm u22
incus ls
----
