= Server setup
:sectnums:

For the first example we have been working on a local directory
([path]`/var/test/`). However for the rest of the sections it is
recommended to work on a server on cloud where you have root access.

== Virtual Private Server

You can get a small and cheap VPS from:

* https://www.netcup.eu/vserver/vps.php
* https://contabo.com/en/vps/
* https://www.hetzner.com/cloud
* etc. (some providers even offer a free VPS instance for an
  evaluation time, if you are not their client yet)

Here are some instructions on how to setup your VPS properly (assuming
that you have installed Ubuntu or Debian):

. xref::howto/basic-server-setup.adoc[window=_blank]
. xref::howto/secure-the-server.adoc[window=_blank]
. xref::howto/install-docker.adoc[window=_blank]

== Dedicated Root Server

Alternatively (and recommended), you can get a Dedicated Root Server
from Hetzner: xref::howto/dedicated-rootserver.adoc[window=_blank]

This option might be a bit more expensive than a small VPS, and might
require more work to setup and maintain, but it offers you much more
space and opportunities for installing lots of applications,
expecially if you need to use Incus containers as well, besides Docker
containers.

TIP: Use a small VPS for testing and getting started, and once you are
familiar and feel confident, migrate to a dedicated root server.
