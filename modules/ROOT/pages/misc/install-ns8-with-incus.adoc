= Install NS8 with Incus
:sectnums: true
:toc:
:toclevels: 4
:page-toclevels: 3

== Installing NS8 (with extra public IP)

****
NOTE: Using an extra IP is cleaner and less complicated, but it is a
bit more expensive. Namely, it requires an extra public IP to be
purchased from Hetzner. All the network traffic for this IP will be
sent to the NS8 container.
****

=== Create a container

https://docs.nethserver.org/projects/ns8/en/latest/index.html[NS8^]
can be installed on a Debian 12 server. Its services are containerized
(using Docker, Podman, etc.) So, lets start by creating a debian12 LXD
container, with the configuration options that allow an efficient
usage of docker containers inside it:

[source,bash]
----
incus init images:debian/12 ns8 \
    -c security.nesting=true \
    -c security.syscalls.intercept.mknod=true \
    -c security.syscalls.intercept.setxattr=true

incus ls
incus config show ns8 --expanded
incus config device show ns8
----

We want to set up an *ipvlan* network interface (nic) instead of a
*bridged* one. More details
https://linuxcontainers.org/incus/docs/main/reference/devices_nic/#nictype-ipvlan[here^].

So, let's correct it:

[source,bash]
----
EXTRA_IP=1.2.3.4/28
EXTRA_GW=4.3.2.1

incus config device add ns8 eth0 nic \
    nictype=ipvlan \
    mode=l2 \
    parent=enp35s0 \          <1>
    name=eth0 \               <2>
    ipv4.address=$EXTRA_IP \
    ipv4.gateway=$EXTRA_GW

incus config show ns8 --expanded
incus config device show ns8
----

<1> Name of the interface on the host.
<2> Name of the interface on the container.

=== Network configuration

Let's start the container and make sure that it has a proper network
configuration:

[source,bash]
----
incus start ns8
incus shell ns8

EXTRA_IP=1.2.3.4/28
EXTRA_GW=4.3.2.1

cat <<EOF > /etc/systemd/network/eth0.network
[Match]
Name=eth0

[Address]
Address=$EXTRA_IP

[Route]
Gateway=$EXTRA_GW

[Network]
DHCP=no
DNS=8.8.8.8
EOF

cat /etc/systemd/network/eth0.network
systemctl restart systemd-networkd

ip addr
ip ro
ping 8.8.8.8

exit
----

We can test with [cmd]`ping` and with [cmd]`nc` that we are able to
access the container from outside, using the extra public IP.

== Installing NS8 (without extra IP)

This kind of setup is a bit more complex, because we need to forward
some ports from the host to the container (like HTTP/HTTPS, SMTP,
IMAP, POP3, etc.)

NOTE: It does not work if we have another mailserver on the host
(which is using the ports for IMAP, POP3, etc.)

=== Create a container

https://docs.nethserver.org/projects/ns8/en/latest/index.html[NS8^]
can be installed on a Debian 12 server. Its services are containerized
(using Docker, Podman, etc.) So, lets start by creating a debian12
Incus container, with the configuration options that allow an
efficient usage of docker containers inside it:

[source,bash]
----
incus launch images:debian/12 ns8 \
    -c security.nesting=true \
    -c security.syscalls.intercept.mknod=true \
    -c security.syscalls.intercept.setxattr=true
incus ls
----

=== Networking

==== Set a fixed IP

We need to set a fixed IP to the NS8 container:

[source,bash]
----
IP=10.24.177.206/24
GW=10.24.177.1

cat <<EOF > /etc/systemd/network/eth0.network
[Match]
Name=eth0

[Address]
Address=$IP

[Route]
Gateway=$GW

[Network]
DHCP=no
DNS=8.8.8.8
EOF

cat /etc/systemd/network/eth0.network
systemctl restart systemd-networkd

ip addr
ip ro
ping google.com
----

==== Forward the ports 80/443

On the host, we need to add a SNI proxy configuration for the domain
`ns8.example.org`, so that the ports *`80/443`* are forwarded to the
container:

[source,bash]
----
cd /var/ds/sniproxy/
vim etc/sniproxy.conf    <1>
ds restart
----

<1> On *[path]`etc/sniproxy.conf`* add lines like these:
+
[subs="+quotes"]
....
table {
    # . . . . .
    
    ### container: ns8##
    ##ns8.example.org   10.24.177.206##

    # . . . . .
}
....

==== Forward other ports

On the host, we also need to forward to the container the Wireguard
port, LDAP ports, and email ports:

[source,bash,subs="+quotes"]
----
PUBLIC_IP=1.2.3.4
CONTAINER_IP=10.11.12.13

incus network forward show incusbr0 $PUBLIC_IP

# wireguard port
incus network forward port add \
    incusbr0 $PUBLIC_IP \
    ##udp 55820## \
    $CONTAINER_IP

# LDAP ports
incus network forward port add \
    incusbr0 $PUBLIC_IP \
    ##tcp 389,636## \
    $CONTAINER_IP

# mail server ports 
incus network forward port add \
    incusbr0 $PUBLIC_IP \
    ##tcp 25,465,587,110,143,4190,993,995## \
    $CONTAINER_IP

incus network forward show incusbr0 $PUBLIC_IP
----

== Cosmetic improvements

Let's make some small improvements inside the container:

[source,bash]
----
incus shell ns8

sed -i /etc/vim/vimrc \
    -e 's/^"set background=dark/set background=dark/'

sed -i ~/.bashrc \
    -e 's/^# alias/alias/' \
    -e 's/^# export/export/'

cat <<'EOF' >> ~/.bashrc
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
EOF

sed -i /etc/bash.bashrc \
    -e '/enable bash completion/,+7 s/^#//' \
    -e '/enable bash completion/ s/^/#/'
apt install -y bash-completion

source ~/.bashrc
----

NOTE: These are just cosmetic modifications. They are not strictly
required, but improve our experience while we work inside the
container.

== Set a FQDN hostname

Let's also set a FQDN hostname inside the container:

[source,bash]
----
FQDN=ns8.example.org
echo $FQDN > /etc/hostname
hostname $FQDN
hostname
----

Let's add the FQDN at [path]`/etc/hosts` as well:

[source,bash]
----
sed -i /etc/hosts \
    -e "s/ns8.*/ns8  $FQDN/"
----

== Install NS8

NOTE: Instructions here:
https://docs.nethserver.org/projects/ns8/en/latest/install.html

[source,bash]
----
apt install -y curl
curl https://raw.githubusercontent.com/NethServer/ns8-core/ns8-stable/core/install.sh | bash
----

Then open this in an incognito browser tab:
https://ns8.example.org/cluster-admin/ It still does not have an SSL
certificate, so we have to accept the exception.

Use the username `admin` and password `Nethesis,1234` to login.

== Basic setup of NS8

. Create a new cluster.
. Change the admin password.
. Set the correct Domain.
. Go to "Settings -> TLS certificates" and get an SSL certificate for
  the FQDN `ns8.example.org`.

