#!/bin/bash

help() {
    cat <<EOF
Functions:
    hl_bash [<file.sh>]
    hl_conf [<file.conf>]
        Display a bash or a config file highlighted.
EOF
}

[[ $(basename $0) == 'lib.sh' ]] && help && exit

# get the directory of testing in a variable
TEST_DIR=$(dirname $(realpath $0))
cd $TEST_DIR

# restart the script with './play'
# unless the first option is 'noplay'
case $1 in
    noplay|playing)
        shift
        ;;
    auto|autoplay)
        shift
        exec sudo env DELAY=1.5 ./play $0 playing "$@"
        ;;
    *)
        shift
        exec sudo ./play $0 playing "$@"
        ;;
esac

hl_bash() {
    highlight -S bash -O xterm256 $1
}

hl_conf() {
    highlight -S ini -O xterm256 $1
}

##################### start #####################

[[ -n $TITLE ]] && echo "TITLE='$TITLE'" | hl_bash
[[ -n $DESCRIPTION ]] && echo "DESCRIPTION='$DESCRIPTION'" | hl_bash

# start verbose mode
set -o verbose

## Press [Enter] to continue
