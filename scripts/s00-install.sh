#!/bin/bash -i

TITLE="Install docker-scripts"
DESCRIPTION=""

source $(dirname $0)/lib.sh

#
# Install dependencies
#
##

:; apt install --yes m4 git

#
# Get the code from GitLab
#
##

:; git clone https://gitlab.com/docker-scripts/ds /opt/docker-scripts/ds

#
# Install it
#
##

:; cd /opt/docker-scripts/ds/

:; make install

#
# Run it
#
##

:; ds

:; ds -h
